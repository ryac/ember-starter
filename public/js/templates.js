Ember.TEMPLATES["application"] = Ember.Handlebars.compile("<header>\n  <nav class=\"top-bar\">\n    <ul class=\"title-area\">\n      <li class=\"name\">\n        <h1><a href=\"#\">App Name</a></h1>\n      </li>\n    </ul>\n  </nav>\n</header>\n<div id=\"container\">\n  {{outlet}}\n</div>");

Ember.TEMPLATES["loading"] = Ember.Handlebars.compile("<p>LOADING...</p>");

Ember.TEMPLATES["posts"] = Ember.Handlebars.compile("<div class=\"row\">\n  <div class=\"columns small-12 large-6\">\n    <h3>Recent Posts yo..</h3>\n    <hr/>\n    <ul>\n      {{#each model}}\n        <li>{{title}}</li>\n      {{/each}}\n    </ul>\n    <div class=\"columns small-12 large-6\">\n      {{outlet}}\n    </div>\n  </div>\n</div>");